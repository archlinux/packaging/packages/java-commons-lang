# Maintainer:
# Contributor: Jonas Witschel <diabonas@archlinux.org>

pkgname=java-commons-lang
pkgver=3.17.0
pkgrel=1
pkgdesc='A host of helper utilities for the java.lang API'
arch=('any')
url='https://commons.apache.org/proper/commons-lang/'
license=('APACHE')
depends=('java-runtime-headless')
makedepends=('java-environment' 'maven')
source=("https://archive.apache.org/dist/commons/lang/source/commons-lang3-$pkgver-src.tar.gz"{,.asc})
sha512sums=('e633b0caeb9556c68384c2bf20e374fbac910b9979b25774c632e50c1bec41e97c14362978dc092c8b5859291e54fe51e76ad7a61c9b2efbe1e4538f46c1e3ee'
            'SKIP')
validpgpkeys=('B6E73D84EA4FCC47166087253FAAD2CD5ECBB314'  # Rob Tompkins <chtompki@apache.org>
              '2DB4F1EF0FA761ECC4EA935C86FDC7E2A11262CB') # Gary David Gregory (Code signing key) <ggregory@apache.org>

build() {
	cd "commons-lang3-$pkgver-src"

	# Building with JDK 17 currently leads to a test failure of
	# "ToStringBuilderTest"
	export PATH="/usr/lib/jvm/java-${_java}-openjdk/bin:$PATH"

	mvn -am \
		-Dproject.build.outputTimestamp="$SOURCE_DATE_EPOCH" \
		-Dmaven.javadoc.skip=true \
		-Dmaven.test.skip=true \
		clean package
}

check() {
	cd "commons-lang3-$pkgver-src"
	export PATH="/usr/lib/jvm/java-${_java}-openjdk/bin:$PATH"
	mvn test
}

package() {
	cd "commons-lang3-$pkgver-src"
	install -Dm644 "target/commons-lang3-$pkgver.jar" -t "$pkgdir/usr/share/java/commons-lang"
	ln -s "commons-lang3-$pkgver.jar" "$pkgdir/usr/share/java/commons-lang/commons-lang.jar"
}
